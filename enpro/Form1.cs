﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Translate;

namespace enpro
{
    public partial class Form1 : Form
    {
        string send(string text)
        {
            Translate.Google.Translate_Get("", "fa", text);
            return Google.translate;
        }
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
           // trtxt.Visible = false;
            //txt.Visible = false;           
           // button4.Visible = false;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            trtxt.Clear();
            trtxt.Visible = true;

            string text = entxt.Text;
            #region part1
            string ply = "played";
            string go = "gone";
            string tr = "travelled";
            string hlp = "helped";
            string wrk = "worked";
            #endregion
            #region part2
            string kne = "a knee injury";
            string sik = "a sickness";
            string mon = "money";
            string lak = "lack of time";
            string laz = "laziness";
            #endregion
            string fh = "he would have ";
            string shf = "she would have ";
            string rul = " if it was not for ";
            txt.Text = rul;
            if (text.Contains(fh) || text.Contains(shf))
            {
                if (text.Contains(ply))
                {
                    if (text.Contains(kne))
                    {
                        
                        trtxt.Text = "اگر به خاطر جراحت پا نبود،بازی کرده بود.";
                        string send = fh + ply + rul + kne;
                        entxt.Text += Environment.NewLine + "-----------------------" + Environment.NewLine+
                            send;
                    }                  
                }
                else if (text.Contains(go))
                {
                    if (text.Contains(sik))
                    {
                        trtxt.Text = "اگر به خاطر بیماری نبود، رفته بود.";
                        string send = fh + go + rul + sik;
                        entxt.Text += Environment.NewLine + "-----------------------" + Environment.NewLine +
                            send;
                    }                   
                }
                else if (text.Contains(tr))
                {
                    if (text.Contains(mon))
                    {
                        trtxt.Text = "اگر به خاطر پول نبود ، سفر رفته بود.";
                        string send = fh + tr + rul + mon;
                        entxt.Text += Environment.NewLine + "-----------------------" + Environment.NewLine +
                            send;
                    }                   
                }
                else if (text.Contains(hlp))
                {

                    if (text.Contains(lak))
                    {
                        trtxt.Text = "اگر به خاطر کمبود وقت نبود کمک کرده بود.";
                        string send = fh + hlp + rul + lak;
                        entxt.Text += Environment.NewLine + "-----------------------" + Environment.NewLine +
                            send;
                    }                   
                }
                else if (text.Contains(wrk))
                {

                    if (text.Contains(laz))
                    {
                        trtxt.Text = " اگر به خاطر تنبلی نبود، کار کرده بود.";
                        string send = fh + wrk + rul + laz;
                        entxt.Text += Environment.NewLine + "-----------------------" + Environment.NewLine +
                            send;
                    }                   
                }
                else
                {
                    trtxt.Text += "\n\nمتن مورد نظر یافت نشد!!";

                }
            }
            else
            {
                trtxt.Text += "\n\nمتن مورد نظر یافت نشد!!";
                txt.Enabled = true;
                button4.Enabled = true;
                
               
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            trtxt.Text = send(txt.Text);
        }
        private void button5_Click(object sender, EventArgs e)
        {
            string newtext = entxt.Text.Replace("but for", "if it was not for ");

            entxt.Text += Environment.NewLine + entxt.Text.Replace("but for", "if it was not for ");
            //MessageBox.Show( send(entxt.Text.Replace("but for", "if it was not for ")));
            string tr = send(newtext);
            if (tr.Contains("اگر اگر "))
            {
                string f = tr.Replace("اگر اگر", "اگر به خاطر ");
                //MessageBox.Show(f, "Google said:");
                trtxt.Clear();
                trtxt.Text = f;

            }
            else if(tr.Contains("اگر"))
            {
                string f = tr.Replace("اگر", "اگر به خاطر ");
                //MessageBox.Show(f, "Google said:");
                trtxt.Clear();
                trtxt.Text = f;


            }
            else
            {
                trtxt.Clear();
                trtxt.Text = tr;
            }

            //MessageBox.Show(f,"Google said:");
        }        
        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("by Alex", "Info ", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            entxt.Clear();
        }

       

        private void txt_Click(object sender, EventArgs e)
        {
            //txt.Clear();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
